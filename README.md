
<div align="center">
<img src="https://bitbucket.org/Ph0enixKM/vsc-magma-language/raw/86414b15804dfe1f86db8dba057e67d3a06ad473/images/icon.png" width="150">
</div>

# Magma syntax support extension

## How to use?

1. Create your Magma source file

> <img src="https://bitbucket.org/Ph0enixKM/vsc-magma-language/raw/86414b15804dfe1f86db8dba057e67d3a06ad473/images/icon.png" width="15"> &nbsp; app.mg

1. Once you have that feel free to write your magma code
2. Report any issues or bugs - it will help us developing the extension
## Preview syntax

![Preview Syntax](https://bitbucket.org/Ph0enixKM/vsc-magma-language/raw/5e202775202f4f1a1428d40b3258fb86b0449986/images/example.png)

## Release Notes

### 1.0.0

Initial release of Magma-Language

-----------------------------------------------------------------------------------------------------------
